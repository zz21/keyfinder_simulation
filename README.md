# Nom du projet
Simulation d'un keyfinder

## Objectif
Ce projet a pour objectif de simuler un keyfinder en utilisant les fonctionnalités de la carte nRF52Dk de NORDIC SEMICONDUCTOR. 

# Comment démarrer le projet ? 
Pour démarrer le projet, vous devez cloner le dépôt Git et avoir installé sur votre PC le logiciel *nRF Connect for Desktop*. Dans ce dernier, téléchargez la section *Toolchain Manager*. Une fois cela fait, rendez-vous sur la dernière version de *Visual Studio Code* pour télécharger toutes les extensions suivantes : *nRF kconfig*, *nRF DeviceTree*, *nRF Terminal*, *nRF Connect for VS Code* et *nRF Connect for VS Code Extension Pack*. Une fois tout ceci fait, ouvrez le projet grâce à l'extension nRF Connect, puis créez un build pour compiler le projet (choisissez le modèle pour la carte : nRF52Dk52832). Enfin, appuyez sur le bouton *flash* pour téléverser le programme sur la carte.

Pour disposer de toutes les fonctionnalités du projet, entre autre les différents services, vous devez également avoir l'application nRF Connect sur votre téléphone.

# Les fonctionnalités
Les fonctionnalités proposées par mon application sont des simulations simples d'un keyfinder.

**Fonctionnalité 1 : simulation du bouton du téléphone portable**
Lorsque l'on veut retrouver le keyfinder grâce à son téléphone, on appuie sur un bouton sur notre téléphone, et ainsi notre keyfinder se met à sonner. Alors, pour simuler le bouton, j'ai implémenté un service nommé *Interact* qui contient une caractéristique bouton qui prend deux valeurs : 0x00 lorsque l'on ne cherche pas le keyfinder et 0x01 lorsqu'on le cherche. C'est ce bouton qui me permet de réaliser la première fonctionnalité.

**Fonctionnalité 2 : simulation du buzzer**
Lorsqu'on a activé le bouton dont on a parlé précédemment, le keyfinder doit sonner. Cependant, la carte n'étant pas dotée d'un haut-parleur, la simulation du buzzer se fait grâce à la LED1 qui augmente de luminosité petit à petit (lorsque le bouton vaut 0x01 seulement).

**Fonctionnalité 3 : simulation du bouton du keyfinder**
Le keyfinder est aussi doté d'un bouton. Pour le simuler, j'ai utilisé le bouton 1 de la carte.

**Fonctionnalité 4 : simulation de la LED**
Lorsqu'on appuie sur le bouton du keyfinder, donc celui de la fonctionnalité 3, une LED s'allume. Cette même LED s'allume aussi lorsque l'on appui sur le bouton de la fonctionnalité 1. J'ai choisi d'utiliser la LED2 pour le simuler.

## Les fichiers

**Le fichier *uuid.h***
Il contient les déclarations des différents UUID utilisés pour les services et caractéristiques.

**Les fichiers *services.c* et *services.h***
Ces fichiers permettent de déclarer les différents services. Il y a aussi toutes les déclarations des structures des caractéristiques et des fonctions permettant la lecture ou l'écriture des caractéristiques.

**Le fichier *Led.h***
Ce n'est pas moi qui ai réalisé ce fichier, mais je l'ai récupéré du module Zephyr. Il contient les fonctions permettant de gérer les LEDs et boutons de la carte.

**Le fichier *main.c***
Ce fichier contient la fonction main, dont le fonctionnement est expliqué ci-dessous.

La fonction main commence par configurer le Bluetooth et initialiser la gestion des structures des caractéristiques des services Alert et Interact. Ensuite, on rentre dans une boucle infinie qui à chaque étape vérifie, avec une pause de 1 seconde :

1. Que vaut la valeur de la structure de la caractéristique Bouton. Si elle vaut 0x01, on recherche alors le keyfinder avec le téléphone. On gère alors l'allumage de la LED et du Buzzer. Sinon, on éteint tout.

2. Est-ce qu'il y a appui sur le bouton du keyfinder (le bouton 1 de la carte). Si oui, on allume la LED.

## Documents annexes
Dans le projet, il y a un document annexe. C'est un tableau Excel regroupant différents services. Il est essentiel pour repérer les services et caractéristiques disponibles sur l'application nRF Connect, puisque je n'arrive pas à afficher les noms.

## Limitations
La première limitation de mon application est que le bouton de la fonctionnalité 3 est censé faire sonner le téléphone aussi. 
Une deuxième limitation est, comme dit précédemment, les noms des services qui n'apparaissent pas.
 



