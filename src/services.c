#include <errno.h>
#include <zephyr/init.h>
#include <zephyr/sys/__assert.h>
#include <stdbool.h>
#include <zephyr/types.h>

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/services/bas.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/printk.h>
#include <zephyr/toolchain/gcc.h>
#include <stdbool.h>
#include <zephyr/logging/log.h>

#include "uuid.h"
#include "services.h"


/* Déclaration de la structure pour la caractéristique LED  */
uint8_t led_state = 0;

/* Fonction de lecture de la caractéristique LED */
ssize_t read_led_control(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset)
{
    return bt_gatt_attr_read(conn, attr, buf, len, offset, &led_state, sizeof(led_state));
}

/* Déclaration de la structure pour la caractéristique Buzzer  */
uint8_t buzzer_state = 0;

/* Fonction de lecture de la caractéristique Buzzer */
ssize_t read_buzzer_state(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset)
{
    return bt_gatt_attr_read(conn, attr, buf, len, offset, &buzzer_state, sizeof(buzzer_state));
}

/* Déclaration du service Alert */
BT_GATT_SERVICE_DEFINE(alert_svc,
    BT_GATT_PRIMARY_SERVICE(BT_UUID_ALERT),
    BT_GATT_CHARACTERISTIC(BT_UUID_BUZZER, BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
                           BT_GATT_PERM_READ | BT_GATT_PERM_WRITE, read_buzzer_state, NULL, &buzzer_state),
    BT_GATT_CHARACTERISTIC(BT_UUID_LED, BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
    BT_GATT_PERM_READ | BT_GATT_PERM_WRITE, read_led_control,NULL, &led_state),
);



/* Déclaration de la structure pour la caractéristique Bouton (du telephone)*/
uint8_t button_state = 0;

/*Déclaration de la structure pour la caractéristique du bouton simulant celui du keyfinder (donc celui situé sur la carte)*/
uint8_t button_keyfinder = 0;

/* Fonction de lecture des caractéristiques Bouton*/
ssize_t read_button_state(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset)
{
    uint8_t state = button_state;
    return bt_gatt_attr_read(conn, attr, buf, len, offset, &state, sizeof(state));
}

/* Fonction de lecture des caractéristiques Bouton du keyfinder (celui situé sur la carte)*/
ssize_t read_buttonkeyfinder_state(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset)
{
    uint8_t state = button_keyfinder;
    return bt_gatt_attr_read(conn, attr, buf, len, offset, &state, sizeof(state));
}

/* Fonction d'écriture de la caractéristique Bouton*/
ssize_t write_button_state(struct bt_conn *conn, const struct bt_gatt_attr *attr,const void *buf, uint16_t len, uint16_t offset)
{
    uint8_t data;
    memcpy(&data, buf, sizeof(data));
    button_state = data;
    return len;
}

/* Déclaration du service Interact */
BT_GATT_SERVICE_DEFINE(interact_svc,
    BT_GATT_PRIMARY_SERVICE(BT_UUID_INTERACT),
    BT_GATT_CHARACTERISTIC(BT_UUID_BOUTON, BT_GATT_CHRC_READ | BT_GATT_CHRC_WRITE,
                           BT_GATT_PERM_READ | BT_GATT_PERM_WRITE, read_button_state, write_button_state, &button_state),
    BT_GATT_CHARACTERISTIC(BT_UUID_KEYFINDERBOUTON,BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
                            BT_GATT_PERM_READ ,read_buttonkeyfinder_state,NULL,&button_keyfinder)
);

/* Déclaration de la structure pour la caractéristique version (du telephone)*/
static const char *APP_VERSION = "1.0.0";

/* Fonction de lecture de la caractéristique Version */
ssize_t read_version(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset)
{
    return bt_gatt_attr_read(conn, attr, buf, len, offset, APP_VERSION,strlen(APP_VERSION));
}

/* Déclaration du service System */
BT_GATT_SERVICE_DEFINE(system_svc,
    BT_GATT_PRIMARY_SERVICE(BT_UUID_SYSTEM),
    BT_GATT_CHARACTERISTIC(BT_UUID_VERSION, BT_GATT_CHRC_READ,
                           BT_GATT_PERM_READ, read_version, NULL, NULL)
);



/* Déclaration de la structure pour la caractéristique Debug */
static const char *debug_string = "Debug";

/* Fonction de lecture de la caractéristique Debug */
ssize_t read_debug_string(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset)
{
    return bt_gatt_attr_read(conn, attr, buf, len, offset, debug_string,strlen(debug_string));
}

/* Déclaration du service Development */
BT_GATT_SERVICE_DEFINE(development_svc,
    BT_GATT_PRIMARY_SERVICE(BT_UUID_DEVELOPMENT),
    BT_GATT_CHARACTERISTIC(BT_UUID_DEBUG, BT_GATT_CHRC_READ,
                           BT_GATT_PERM_READ, read_debug_string, NULL, NULL)
);


/* Déclaration de la structure pour la caractéristique Advertising*/
uint8_t advertising_data[1] = {0xAA};

ssize_t read_advertising(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset)
{
    return bt_gatt_attr_read(conn, attr, buf, len, offset,advertising_data,strlen(advertising_data));
}


BT_GATT_SERVICE_DEFINE(communication_svc,
    BT_GATT_PRIMARY_SERVICE(BT_UUID_COMMUNICATION),
    BT_GATT_CHARACTERISTIC(BT_UUID_ADVERTISING,
                           BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
                           BT_GATT_PERM_READ, read_advertising, NULL,advertising_data),
);   