#include <zephyr/types.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <zephyr/sys/printk.h>
#include <zephyr/sys/byteorder.h>
#include <zephyr/kernel.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/devicetree.h>
#include <zephyr/device.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/drivers/pwm.h>
#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/hci.h>
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/gatt.h>

#include "uuid.h"
#include "services.h"
#include "LED.h"

#define NUM_STEPS 50U	//Utilisé pour l'allumage progressive de la LED simulant le buzzer		
#define LED2_PIN 18		
#define BOUTON1_PIN 13				

static const struct bt_data ad[] = {
	BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
	BT_DATA_BYTES(BT_DATA_UUID16_ALL,
				BT_UUID_16_ENCODE(BT_UUID_ALERT_VAL),
				BT_UUID_16_ENCODE(BT_UUID_INTERACT_VAL),
				BT_UUID_16_ENCODE(BT_UUID_SYSTEM_VAL),
				BT_UUID_16_ENCODE(BT_UUID_COMMUNICATION_VAL),
				BT_UUID_16_ENCODE(BT_UUID_DEVELOPMENT_VAL)
				)
};

extern uint8_t button_state;
extern uint8_t button_keyfinder;
extern uint8_t buzzer_state;
extern uint8_t led_state;
static const struct pwm_dt_spec pwm_led0 = PWM_DT_SPEC_GET(DT_ALIAS(pwm_led0));


static void connected(struct bt_conn *conn, uint8_t err)
{
	if (err) {
		printk("Connection failed (err 0x%02x)\n", err);
	} else {
		printk("Connected\n");
	}
}

static void disconnected(struct bt_conn *conn, uint8_t reason)
{
	printk("Disconnected (reason 0x%02x)\n", reason);
}

BT_CONN_CB_DEFINE(conn_callbacks) = {
	.connected = connected,
	.disconnected = disconnected,
};

static void bt_ready(void)
{
	int err;

	printk("Bluetooth initialized\n");

	err = bt_le_adv_start(BT_LE_ADV_CONN_NAME, ad, ARRAY_SIZE(ad), NULL, 0);
	if (err) {
		printk("Advertising failed to start (err %d)\n", err);
		return;
	}

	printk("Advertising successfully started\n");
}

static void auth_cancel(struct bt_conn *conn)
{
	char addr[BT_ADDR_LE_STR_LEN];

	bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

	printk("Pairing cancelled: %s\n", addr);
}

static struct bt_conn_auth_cb auth_cb_display = {
	.cancel = auth_cancel,
};

void buzzer_init(uint32_t * pulse_width, uint32_t * step, uint8_t * dir)
{
	*pulse_width = 0U;
	*step = pwm_led0.period / NUM_STEPS;
	*dir = 1U;
	device_is_ready(pwm_led0.dev);
}

void led_init()
{
	nrf_gpio_cfg_output(LED2_PIN);
	nrf_gpio_pin_set(LED2_PIN);
}

void bouton_init()
{
	nrf_gpio_cfg_input(BOUTON1_PIN, NRF_GPIO_PIN_PULLUP);
}


int main(void)
{
	int err;
	int ret;
	int cligno = 0;
	uint32_t pulse_width;
	uint32_t step;
	uint8_t dir;

	err = bt_enable(NULL);
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return 0;
	}

	bt_ready();

	bt_conn_auth_cb_register(&auth_cb_display);

	/*initialisation du buzzer*/
	buzzer_init(&pulse_width,&step,&dir);
	/*initialisation de la LED*/
	led_init();
	/*initialisation du bouton sur la carte*/
	bouton_init();

	while (1)
	{
		k_sleep(K_SECONDS(1));
		if (button_state == 1)									
		{
			buzzer_state = 1;
			led_state = 1;
			ret = pwm_set_pulse_dt(&pwm_led0, pulse_width);			//on réalise le pulse 
			if (dir) 
			{
				pulse_width += step;								//augmentation de la luminiosité
				if (pulse_width >= pwm_led0.period)					//on s'arrete quand on a atteind le max
				{
					dir = 0U;
				}
			} 
			if(cligno)												//puis on fait clignotter la LED
			{
				nrf_gpio_pin_clear(LED2_PIN);	
			}
			else
			{
				nrf_gpio_pin_set(LED2_PIN);
			}
			cligno = !cligno;
		}
		else if (button_state == 0)
		{
			buzzer_state = 0;
			dir = 1U;
			pulse_width = 0U;
			ret = pwm_set_pulse_dt(&pwm_led0,pulse_width);			//mise en off du buzzer
			if (button_keyfinder !=1)
			{
				nrf_gpio_pin_set(LED2_PIN);								//pareil pour la led
				led_state = 0;
			}
		}

		if(!nrf_gpio_pin_read(BOUTON1_PIN))
		{
			button_keyfinder = !button_keyfinder;

			nrf_gpio_pin_clear(LED2_PIN);						//allumage de la LED
			led_state = 1;	
		}
	}
	return 0;
}
